// Setup the dependencies
const express = require("express");
const mongoose = require ("mongoose");

// Import the task routes
const taskRoute = require("./routes/taskRoute")

// Server setup
const app = express();
const port = 3001;
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://dbajleyson:XCqXRTUTKYmZVob8@wdc028-course-booking.kpkh5.mongodb.net/b138_to-do?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Add the task route
app.use("/tasks", taskRoute)
// localhost:3001/tasks

// Serving Listening
app.listen(port, () => console.log(`Now listening to port ${port}`));